#!/usr/bin/python3

import argparse

class Subtree:
    """ Representation of a tree """
    
    def __init__(self, level):
            self.level = level
            self.parent = None
            self.children = []
            self.name = ''
            self.length = 0

    def __str__(self):
        output = ''

        if len(self.children) > 0:

            output += "("

            for child in self.children:
                output += str(child)
           
            output = output[:-1]
            output += ")"

        if self.name is not None:
            output += self.name

        if float(self.length) > 0.0:
            output += ":" + str(self.length)

        if self.parent == None and self.level == 0:
            output += ';'
        else:
            output += ','
        
        return output


def readInSubtree(level, parent, tree):
    """ Process one (sub)tree in newick format """

    subtree = Subtree(level)
    subtree.parent = parent

    children = []
    name = ''
    length = 0

    child_name = ''
    child_length = 0
    temporary_buffer = ''
    no_other_element = False
    length_trigger = False

    pos = 0

    if tree[pos] == '(':
        pos+=1

        while tree[pos] is not ')':

            if tree[pos] == '(':
                # There is a new subtree
                child_subtree, offset = readInSubtree(level+1, subtree, tree[pos:])
                children.append(child_subtree)
                pos+=offset

                if tree[pos] == ')':
                    # child subtree was the last element of this subtree
                    no_other_element = True
                else:
                    # Otherwise move after the comma as usual
                    pos += 1

            elif tree[pos] == ':':
                # name of child is read in
                child_name = temporary_buffer
                length_trigger = True
                temporary_buffer = ''
                pos+=1

            elif tree[pos] == ',':
                # child description ends
                if length_trigger:
                    child_length = float(temporary_buffer)
                else:
                    child_name = temporary_buffer

                child = Subtree(level+1)
                child.parent = subtree
                child.name = child_name
                child.length = child_length
                children.append(child)

                child_length = 0
                child_name = ''
                temporary_buffer = ''
                length_trigger = False
                pos+=1
            
            else:
                # read in chars (name or length of child)
                temporary_buffer += tree[pos]
                pos+=1


        if not no_other_element:
            if length_trigger:
                child_length = float(temporary_buffer)
            else:
                child_name = temporary_buffer

            child = Subtree(level+1)
            child.parent = subtree
            child.name = child_name
            child.length = child_length
            children.append(child)

            temporary_buffer = ''
            length_trigger = False

        pos+=1 # set position after ')'

    if level > 0:
        closing_char = ','
    else:
        closing_char = ';'

    while tree[pos] != ')' and tree[pos] != closing_char:
        if tree[pos] == ':':
            name = temporary_buffer
            temporary_buffer = ''
            length_trigger = True
        else:
            temporary_buffer += tree[pos]

        pos+=1

    if length_trigger:
        length = float(temporary_buffer)
    else:
        name = temporary_buffer

    subtree.name = name
    subtree.length = length
    subtree.children = children

    return [subtree, pos]


def main(inputFile, outputFile):
    input_data = open(inputFile[0], 'r')

    trees = []

    for line in input_data: 
        tree, position = readInSubtree(0,None,line)
        trees.append(tree)

    input_data.close()
    output = open(outputFile[0], 'w')

    for tree in trees:
        output.write(str(tree))
        output.write('\n')

    output.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Newick format reader')
    parser.add_argument('infile', nargs=1)
    parser.add_argument('outfile', nargs=1)
    args = parser.parse_args()

    main(args.infile, args.outfile)

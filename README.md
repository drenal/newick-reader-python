## Usage

```
$ ./newick.py input_file output_file
```

Only the first line will be processed currently.

## Example input 

```
a;
```

```
(a,b,c);
```

```
(a,b,(d,e,f,(g,h,i,j,k),l),m)c:5;
```

## References

* https://en.wikipedia.org/wiki/Newick_format
* http://evolution.genetics.washington.edu/phylip/newicktree.html